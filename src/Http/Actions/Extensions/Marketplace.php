<?php

namespace Dcat\Admin\Http\Actions\Extensions;

use Dcat\Admin\Admin;
use Dcat\Admin\Grid\Tools\AbstractTool;
use Dcat\Admin\Widgets\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class Marketplace extends AbstractTool
{
    protected $style = 'btn btn-primary';

    protected $modalId = 'show-app-store';

    public function title()
    {
        return '<i class="feather icon-shopping-cart"></i> &nbsp;' . trans('admin.marketplace');
    }

    public function html()
    {
        $html = parent::html() . '&nbsp;';

        return <<<HTML
{$html}
<div class="modal fade" id="{$this->modalId}" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">{$this->title()}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
HTML;
    }

    public function handle(Request $request)
    {
        if ($store = config('admin.extension.store')) {
            $res = Http::get($store);
            return $this->response()->html($res);
        }
    }

    protected function setupHtmlAttributes()
    {
        // 添加class
        $this->addHtmlClass('btn btn-primary');

        // 保存弹窗的ID
        $this->setHtmlAttribute('data-target', '#'.$this->modalId);

        parent::setupHtmlAttributes();
    }

    public function handleHtmlResponse()
    {
        return <<<'JS'
function (target, html, data) {
    var $modal = $(target.data('target'));

    $modal.find('.modal-body').html(html)
    $modal.modal('show')
}
JS;
    }
}
